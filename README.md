# Simple Todo Application using Python + Angular Microapp + Database
## Disclaimer
The repository outlines development practices to create a binary application (python backend, MySQL database, angular microfrontend) for Industry Cloud NOC Platform - `App Studio framework` 
## Objective: 
This repository outlines guidelines to create a a python binary which consist of Angular Microapp Frontend, Database Migrations, Rest APIs, Rest APIs integration with frontend as a single monolithic application. From this repository, you can learn how
1. to create a Python Application using FastAPI
2. to create Angular Microfrontend and integrate Python Backend
3. database migration works using Python Alembic framework
4. to create singleton/monolithic application (python + angular + database)
5. to create `standalone binary` of monolithic application 

Note: Standalone Binary refers to a single file which contains - Entire Python Runtime, FastAPI backend source code, all dependencies along with Angular compiled/build code. This standalone binary can run on Windows/Linux without even having Python installed in OS)

With the following stated above, we'll create a simple Todo App Binary, where a user can
- Create a new todo item
- Delete a todo item
- View all todo items
- Mark as complete for a todo item

![image](images/todo.PNG)

## Repository Structure
1. Python folder
```
 ───python
    │   .env
    │   alembic.ini
    │   main.py
    │   main.spec
    │   requirements.txt
    │
    ├───alembic
    │   │   env.py
    │   │   README
    │   │   script.py.mako
    │   │
    │   └───versions
    │           fea9bd30c394.py
    │
    ├───app
    │       crud.py
    │       db.py
    │       main.py
    │       models.py
    │
    └───static
```
Let's check the purpose of each file and folder here in this directory
- `main.py`: This file serves as a driver program to run (these operations run in sequence as given below)
    - Database Migrations using [Alembic](!https://pypi.org/project/alembic/): This operation connects to MySQL Database, creates necessary tables, indexes as provided in `alembic` folder(more details here [TODO])
    - Prepare Static Folder: This is the folder where you Angular Build lives
    - [FastAPI](!https://fastapi.tiangolo.com/) WebServer: Boots up the FastAPI webserver
- `app` folder:
    - `crud.py`: All CRUD related business logic is added here. Like connecting a database, create a row, update a row, delete a row, fetch rows etc. 
    - `db.py`: Defines database connection object for MySQL
    - `main.py`: Contains all exposed REST API's and additional logic to make things work as a binary application
    - `models.py`: Defines ORM for tables used
- `alembic` folder: 
    - `version` folder: contains table schema created by `alembic revision -m "table name"` cli command. Read more at [alembic documentation link](!https://alembic.sqlalchemy.org/en/latest/tutorial.html#create-a-migration-script)
    - Refer documentation [here](!https://alembic.sqlalchemy.org/en/latest/tutorial.html) for complete details about `alembic` folder
- `static` folder: this is folder where you'll copy paste all files that were generated in `dist` folder from `angular` folder (more details about `dist` folder here [TODO])


2. Angular folder
```
│   .browserslistrc
│   .editorconfig
│   .gitignore
│   angular.json
│   karma.conf.js
│   package-lock.json
│   package.json
│   tsconfig.app.json
│   tsconfig.json
│   tsconfig.spec.json
│   webpack.config.js
│   webpack.prod.config.js
│
└───src
    │   bootstrap.ts
    │   favicon.ico
    │   index.html
    │   main.ts
    │   polyfills.ts
    │   styles.scss
    │   test.ts
    │
    ├───app
    │   │   app-routing.module.ts
    │   │   app.component.html
    │   │   app.component.scss
    │   │   app.component.spec.ts
    │   │   app.component.ts
    │   │   app.module.ts
    │   │   menu.data.ts
    │   │   permissions.ts
    │   │
    │   └───home
    │           home-routing.module.ts
    │           home.component.html
    │           home.component.scss
    │           home.component.spec.ts
    │           home.component.ts
    │           home.module.ts
    │
    ├───assets
    │       .gitkeep
    │
    └───environments
            environment.prod.ts
            environment.ts
```
Let's check the purpose special files/folders inside this directory
- `src/app/app-routing.module.ts`: This file contains routing details that gets used in Shell app. Reuse the same file and change properties of this file by consulting Rudra developer team.
- `src/app/menu.data.ts`: This file configures menu item in shell app. Reuse the same file and change properties of this file by consulting Rudra developer team.
- `src/app/permissions.ts`: This file serves as permission validator. Reuse the same file in your custom application logic
- `home`: This folder contains custom components. If you wish to create custom application, you can start modifying logic from this folder.
- All files in root folder of `angular` needs to copied as it is for your custom application. Copy the following files for your custom app development
    - `.browserslistrc`
    - `.editorconfig`
    - `.gitignore`
    - `angular.json`
    - `karma.conf.js`
    - `package-lock.json`
    - `package.json`
    - `tsconfig.app.json`
    - `tsconfig.json`
    - `tsconfig.spec.json`
    - `webpack.config.js`
    - `webpack.prod.config.js`

## Setup for running application locally
Follow the setup instructions below
1. Clone the repository in your system
    > git clone https://gitlab.com/ic-app-studio/python-angular.git
2. Install Nodejs Dependencies for Angular App. Navigate to `angular` folder and run (make sure you've node v16.x.x or higher installed)
    - Install all dependencies
      > npm i
    - Create build. This will compile all the code and put compiled files in `dist` folder
      > npm run build
    - Copy all files from `dist` folder and copy it as it is in `python\static` folder

3. Install Python Dependencies for FastAPI App. Navigate to `python` folder and run (make sure you've python 10.x.x or higher installed)
    - Create Virtual Environment
      > python -m .venv venv
    - Activate virtual environment
        - Linux based OS
          > source .venv/bin/activate
        - Windows OS
          > .venv\Scripts\activate
    - Install dependencies
      > pip install -r requirements.txt

4. Setup `.env` file inside `python` folder with following environment variables
    - `ALLOWED_ORIGINS`: This variable is mounted automatically when you deploy your app to App Studio. For testing purpose you can use any appropriate value here. This value controls CORS behavior for FastAPI app
    - `APP_ROUTE`: This variable is mounted automatically when you deploy your app to App Studio. For testing purpose you can use any appropriate value here. For example set the value as `/myapp` then you can access REST API (get all todos) on `http://localhost:8080/myapp/todos`. This variables is used internally within your app for URL rewrite purpose. The URL rewrite middleware is already added in source code - `python\app\main.py: Line 22` (function name is `rewrite_url`). Please reuse the same function in your application logic (THIS IS MUST TO REUSE IN YOUR APP, WITHOUT THIS, THE BINARY WON'T FUNCTION CORRECTLY AFTER DEPLOYMENT ON APP STUDIO).
    - `DB_USERNAME`: This variable is mounted automatically when you deploy your app to App Studio. Create a MySQL Database locally or on Cloud and create a database, user, password. Associate username in this field
    - `DB_PASSWORD`: This variable is mounted automatically when you deploy your app to App Studio. Set Password of your Database as created in earlier step
    - `DB_HOST`: This variable is mounted automatically when you deploy your app to App Studio. Set Host/IP of your MySQL instance
    - `DB_PORT`: This variable is mounted automatically when you deploy your app to App Studio. Set MySQL DB Port of your instance
    - `DB_NAME`: This variable is mounted automatically when you deploy your app to App Studio. Set name of your database, that you created in your MySQL instance
    - `API_HOST`: Set value of this as `http://localhost:8080` when testing locally. When deploying your application to App Studio, set the value of this field as environment variable in your `manifest.yaml` file. To set correct value in manifest, contact Rudra developer team.

    Sample `.env` file
    ```
    ALLOWED_ORIGINS=google.com
    APP_ROUTE=/myapp
    DB_USERNAME=my_user
    DB_PASSWORD=testP@ssw0rd
    DB_HOST=127.0.0.1
    DB_PORT=6104
    DB_NAME=marketplace_todo
    API_HOST=http://localhost:8080
    ```
5. Run FastAPI Webserver by running (make sure virtual environment is active and you're running this command from root of `python` folder)
   > python main.py

6. After running FastAPI Webserver, your Microfrontend is ready to be integrated in Angular Shell App. The `remoteEnty.js` will be available on this url - `https://localhost:8080/myapp/remoteEntry.js`

## Create Application Binary
Once you've completed all the local setup steps, you can move ahead and create binary of your application

1. Navigate to `python` folder. Make sure virtual environment is active 
2. Make sure you've compiled Angular application inside `static` folder
3. Run this command to create binary
   > pyinstaller --add-data="alembic:data/alembic" --add-data="alembic.ini:data/alembic.ini" --add-data="static:data/static" --onefile main.py
4. This will output binary in `dist` folder, this is the final binary
5. *Platform specific binary*: When you run step 3 in Windows OS, it will create `.exe` file. In order to create binary for App Studio, you need to repeat the same process in a Linux based OS to create a Linux compatible binary. You can easily do it inside GCP Cloud Shell
`requirements.txt` is generated in Linux Environment - please use it with caution in Windows OS

