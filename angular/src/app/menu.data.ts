import { Permissions } from './permissions';

export class Menu {
  private permissions = new Permissions();

  public async menuGenrator(hideMenus: boolean) {
    let menus;
    menus = [
      {
        name: 'Todo Sample App',
        visible: this.permissions.isGranted(
          'Pages.AppStudio.TodoApp.Admin'
        ),
        icon: '',
        selected: 'To Todo Sample App',
        isCollapsed: true,
        route: {
          url: ['/todo-app'],
        },
      },
    ];
    return menus;
  }
}
